% compares Ref lap with another lap
% neccessary before: doing the LAPS script with the own laps

%% initialize ref an down lap
start_ref = 189.515 * 200;
start_own = 268.51 * 200;

time_ref = 23;
time_own = 23;

end_ref = start_ref + time_ref / 0.005;
end_own = start_own + time_own / 0.005;

%% reference lap

[file_ref, path_ref] = uigetfile;

load([path_ref file_ref]);

RefLeg = strrep(file_ref, '_', '\_');

RefDistance = cumsum(KF_Geschwindigkeit(start_ref:end_ref) * 0.005/3.6);
RefGP_speed = movmean(diff(Gaspedal) / 0.005, 10);
RefGP_speed(RefGP_speed < 0) = 0;
RefFTT = zeros(length(Gaspedal), 1);
RefFTT(Gaspedal > 0.95 * max(Gaspedal)) = 0.005;
RefBP_speed = abs(movmean(diff(Brakeforce) / 0.005, 10));
RefACC_BE = Fr_ACC_X;
RefACC_BE(RefACC_BE > 0) = 0;

%evaluate time difference over lap
%evaluate distance of Ref lap
distance = cumsum(KF_Geschwindigkeit(start_ref:end_ref)) * 0.005/3.6;

%trash double existing values
[distanceIntRef, indexRef] = unique(distance);

%% figure 1 Ref data Gaspedal

f1 = figure('Name', 'lap comparison: Gaspedal', 'NumberTitle', 'off');
ax1(1) = subplot(3, 1, 1);
ax1(2) = subplot(3, 1, 2);
ax1(3) = subplot(3, 1, 3);

TextRef(1, 1) = "REF LAP:";
TextRef(2, 1) = "laptime - "+ string(round(time_ref, 2)) + "s";
annotation('textbox', [0 0.8 .1 .2], 'String', TextRef, 'EdgeColor', 'none', 'Color', 'r');

subplot(ax1(1));
plot(RefDistance, KF_Geschwindigkeit(start_ref:end_ref), 'r');
ylabel("vehiclespeed"+ newline + "in km/h");
yyaxis right
set(gca, 'YColor', 'k')
plot(RefDistance, cumsum(RefFTT(start_ref:end_ref)), 'r--');
ylim([0, 1 + max(cumsum(RefFTT(start_ref:end_ref)))]);
ylabel("full throttle time"+ newline + "in s");

subplot(ax1(2));
plot(RefDistance, 100 * Gaspedal(start_ref:end_ref), 'r');
ylabel("gaspedalposition"+ newline + "in %");

subplot(ax1(3));
plot(RefDistance, 100 * RefGP_speed(start_ref:end_ref), 'r');
ylabel("gaspedalspeed"+ newline + "in %/s");
xlabel('track distance in metres');

%% figure 2 Ref data Brakepedal

f2 = figure('Name', 'lap comparison: Brakes', 'NumberTitle', 'off');
ax2(1) = subplot(3, 1, 1);
ax2(2) = subplot(3, 1, 2);
ax2(3) = subplot(3, 1, 3);

TextRef2(1, 1) = "REF LAP:";
TextRef2(2, 1) = "laptime - "+ string(round(time_ref, 2)) + "s";
annotation('textbox', [0 0.8 .1 .2], 'String', TextRef2, 'EdgeColor', 'none', 'Color', 'r');

subplot(ax2(1));
plot(RefDistance, KF_Geschwindigkeit(start_ref:end_ref), 'r');
ylabel("vehiclespeed"+ newline + "in km/h");

subplot(ax2(2));
plot(RefDistance, Brakeforce(start_ref:end_ref), 'r');
ylabel("brakeforce"+ newline + "in N");
ylim([0, 1200]);
yyaxis right
set(gca, 'YColor', 'k')
plot(RefDistance, BP_Front(start_ref:end_ref) + BP_Rear(start_ref:end_ref), 'r--');
ylim([0, max(BP_Front(start_ref:end_ref) + BP_Rear(start_ref:end_ref)) + 15]);
ylabel("sum brake pressure"+ newline + "in bar");

subplot(ax2(3));
plot(RefDistance, RefBP_speed(start_ref:end_ref), 'r');
ylabel("brakeforcedeviation"+ newline + "in N/s");
yyaxis right
set(gca, 'YColor', 'k')
plot(RefDistance, movmean(RefACC_BE(start_ref:end_ref), 5) / 9.81, 'r--');
ylim([-2, 0]);
ylabel("brakeeffort"+ newline + "in g");
xlabel('track distance in metres');

%% figure 3 Ref data Traction Circle

f3 = figure('Name', 'lap comparison: traction circle', 'NumberTitle', 'off');
scatter(Fr_ACC_Y(start_ref:end_ref) / 9.81, Fr_ACC_X(start_ref:end_ref) / 9.81, 'r');
xvalue = linspace(-2, 2);
yvalue = sqrt(4 - xvalue.^2);
hold on

for scale = linspace(0.1, 1, 10)
    plot(scale * xvalue, scale * yvalue, 'Color', 1/255 * [100, 100, 100]);
    plot(scale * xvalue, -scale * yvalue, 'Color', 1/255 * [100, 100, 100]);
end

hold off
xlabel('lateral acceleration in g');
ylabel('longitudinal acceleration in g');

%% figure 4 Ref data Steering Wheel and Driving Line

f4 = figure('Name', 'lap comparison: Steering and Driving Line', 'NumberTitle', 'off');
ax4(1) = subplot(3, 1, 1);
ax4(2) = subplot(3, 1, 2);
ax4(3) = subplot(3, 1, 3);

subplot(ax4(1));
plot(RefDistance, KF_Geschwindigkeit(start_ref:end_ref), 'r');
ylabel("vehiclespeed"+ newline + "in km/h");

subplot(ax4(2));
curvature = KF_Geschwindigkeit(start_ref:end_ref) .* KF_Geschwindigkeit(start_ref:end_ref) / (3.6 * 3.6);
curvature = curvature ./ abs(Fr_ACC_Y(start_ref:end_ref));
curvature = curvature.^(-1);
plot(RefDistance, 1000 * movmean(curvature, 20), 'r');
ylabel("curvature"+ newline + "in 1/km");

subplot(ax4(3));
plot(RefDistance, Lenkwinkel(start_ref:end_ref), 'r');
ylabel("steering angle"+ newline + "in degrees");
yyaxis right
set(gca, 'YColor', 'k')
plot(RefDistance, gradient(Lenkwinkel(start_ref:end_ref), 0.005), 'r--');
ylabel("steering speed"+ newline + "in �/s");
xlabel('track distance in metres');

%% figure 5 time difference over lap

f5 = figure('Name', 'lap comparison: time difference', 'NumberTitle', 'off');
ax5(1) = subplot(2, 1, 1);
ax5(2) = subplot(2, 1, 2);

subplot(ax5(1));
plot(RefDistance, KF_Geschwindigkeit(start_ref:end_ref), 'r');
ylabel("vehiclespeed"+ newline + "in km/h");

%% figure 7 Ref data Track Map

f7 = figure('Name', 'lap comparison: track map', 'NumberTitle', 'off');
plot(GPS_Fr_Longitude(start_ref:end_ref), GPS_Fr_Latitude(start_ref:end_ref), 'r');
hold on
iFTT = find(RefFTT > 0);
iFTT(iFTT < start_ref) = [];
iFTT(iFTT > end_ref) = [];
scatter(GPS_Fr_Longitude(iFTT), GPS_Fr_Latitude(iFTT), 'r', '.')
iBP = find(RefBP_speed > 4000);
iBP(iBP < start_ref) = [];
iBP(iBP > end_ref) = [];
scatter(GPS_Fr_Longitude(iBP), GPS_Fr_Latitude(iBP), 'r', 'x')
hold off

%% load own lap (the one that should be compared) and calculations

[file_own, path_own] = uigetfile;

load([path_own file_own])

OwnLeg = strrep(file_own, '_', '\_');

%% own lap
OwnDistance = cumsum(KF_Geschwindigkeit(start_own:end_own) .* Zeitstempel(start_own:end_own)) / 3.6;
OwnGP_velocity = movmean(diff(Gaspedal) / 0.005, 10);
OwnGP_velocity(OwnGP_velocity < 0) = 0;
OwnFTT = zeros(length(Gaspedal), 1);
OwnFTT(Gaspedal > 0.95 * max(Gaspedal)) = 0.005;
OwnBP_speed = abs(movmean(diff(Brakeforce) / 0.005, 10));
OwnACC_BE = Fr_ACC_X;
OwnACC_BE(OwnACC_BE > 0) = 0;

%time difference evaluation over lap
%add own distance
distance = [distance; cumsum(KF_Geschwindigkeit(start_own:end_own) .* gradient(Zeitstempel(start_own:end_own))) / 3.6];

%trash double existing values
[distanceInt, index] = unique(cumsum(KF_Geschwindigkeit(start_own:end_own) .* gradient(Zeitstempel(start_own:end_own))) / 3.6);

%sort and delete double (or more) distance values as basis for interpolation
distance = sort(unique(distance));

% interpolated time for linked distance between laps
% time values results of between original distance, reffering time and the
% interlinked distance (variable: distance)
timeIntRef = interp1(distanceIntRef, (0.005 * (indexRef - 1)), distance);
timeInt = interp1(distanceInt, (0.005 * (index - 1)), distance);

% scaling lap distance for better data fit (own lap regarding to ref lap)
scale_dist = max(RefDistance) / max(OwnDistance);

%% figure 1 Own data
figure(f1)

TextOwn(1, 1) = "OWN LAP:";
TextOwn(2, 1) = "laptime - "+ string(round(time_own, 2)) + "s";
annotation('textbox', [0 0.6 .1 .2], 'String', TextOwn, 'EdgeColor', 'none');

subplot(ax1(1));
hold on
yyaxis left
plot(OwnDistance * scale_dist, KF_Geschwindigkeit(start_own:end_own), 'k');
yyaxis right
plot(OwnDistance * scale_dist, cumsum(OwnFTT(start_own:end_own)), 'k--');
hold off
subplot(ax1(2));
hold on
plot(OwnDistance * scale_dist, 100 * Gaspedal(start_own:end_own), 'k');
hold off
subplot(ax1(3));
hold on
plot(OwnDistance * scale_dist, 100 * OwnGP_velocity(start_own:end_own), 'k');
hold off
legend(RefLeg, OwnLeg);

linkaxes([ax1(1), ax1(2), ax1(3)], 'x');

%% figure 2 Own data
figure(f2)

TextOwn(1, 1) = "OWN LAP:";
TextOwn(2, 1) = "laptime - "+ string(round(time_own, 2)) + "s";
annotation('textbox', [0 0.6 .1 .2], 'String', TextOwn, 'EdgeColor', 'none');

subplot(ax2(1));
hold on
plot(OwnDistance * scale_dist, KF_Geschwindigkeit(start_own:end_own), 'k');
hold off
legend('reference lap', 'own lap');
subplot(ax2(2));
hold on
yyaxis left
plot(OwnDistance * scale_dist, Brakeforce(start_own:end_own), 'k');
yyaxis right
plot(OwnDistance * scale_dist, BP_Front(start_own:end_own) + BP_Rear(start_own:end_own), 'k--');
hold off
subplot(ax2(3));
hold on
yyaxis left
plot(OwnDistance * scale_dist, OwnBP_speed(start_own:end_own), 'k');
yyaxis right
plot(OwnDistance * scale_dist, movmean(OwnACC_BE(start_own:end_own), 5) / 9.81, 'k--');
hold off

linkaxes([ax2(1), ax2(2), ax2(3)], 'x');

%% figure 3 Own data
figure(f3)
hold on
scatter(Fr_ACC_Y(start_own:end_own) / 9.81, Fr_ACC_X(start_own:end_own) / 9.81, 'k');
axis equal
hold off
legend(RefLeg);

%% figure 4 Own data
figure(f4)
subplot(ax4(1));
hold on
plot(OwnDistance * scale_dist, KF_Geschwindigkeit(start_own:end_own), 'k');
hold off

subplot(ax4(2));
hold on
curvatureOwn = KF_Geschwindigkeit(start_own:end_own) .* KF_Geschwindigkeit(start_own:end_own) / (3.6 * 3.6);
curvatureOwn = curvatureOwn ./ abs(Fr_ACC_Y(start_own:end_own));
curvatureOwn = curvatureOwn.^(-1);
plot(OwnDistance * scale_dist, 1000 * movmean(curvatureOwn, 20), 'k');
hold off
legend(RefLeg, OwnLeg);

subplot(ax4(3));
hold on
yyaxis left
plot(OwnDistance * scale_dist, Lenkwinkel(start_own:end_own), 'k');
yyaxis right
plot(OwnDistance * scale_dist, gradient(Lenkwinkel(start_own:end_own), 0.005), 'k--');
hold off

linkaxes([ax4(1), ax4(2), ax4(3)], 'x');

%% figure 5 time difference over lap

figure(f5)

subplot(ax5(1));
hold on
plot(OwnDistance * scale_dist, KF_Geschwindigkeit(start_own:end_own), 'k');
hold off

subplot(ax5(2));
plot(distance, (timeInt - timeIntRef), 'k');
legend('- reference lap');
ylabel("time gap"+ newline + "in s");
xlabel('track distance in metres');

linkaxes([ax5(1), ax5(2)], 'x');

%% figure 7 GPS Own data
figure(f7)
hold on
plot(GPS_Fr_Longitude(start_own:end_own), GPS_Fr_Latitude(start_own:end_own), 'k');
iFTT = find(OwnFTT > 0);
iFTT(iFTT < start_own) = [];
iFTT(iFTT > end_own) = [];
scatter(GPS_Fr_Longitude(iFTT), GPS_Fr_Latitude(iFTT), 'k', '.');
iBP = find(RefBP_speed > 4000);
iBP(iBP < start_own) = [];
iBP(iBP > end_own) = [];
scatter(GPS_Fr_Longitude(iBP), GPS_Fr_Latitude(iBP), 'k', 'x')
hold off
axis off
legend(RefLeg, 'full throttle (FT) phases', 'braking zones');
