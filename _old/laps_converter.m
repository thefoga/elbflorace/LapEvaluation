%% creates laps on basis of GPS data from loaded logdata

% clear old, load new log data
clear all;

[File, Folderpath] = uigetfile;

if File == 0
    warndlg('No data selected! Repeat script start.')
    return
end

Filepath = strcat(Folderpath, File);
load(Filepath);

% initializing
Start = find(GPS_Fr_Longitude > 0.1);
End = length(t);

% length of the data that has to be evaluated
dataset = Start:End;

% Coordinates of Finish Line
% in case of RCC track the line has a fixed longitudinal value
FinLon = 13.6412;
FinLatMin = 51.00103;
% FinLatMin = 51.00107;
FinLatMax = 51.00129;

% get needed logdata area
Lat = GPS_Fr_Latitude(dataset);
Lon = GPS_Fr_Longitude(dataset);

% lap counter
LapsCount = 0;

% time for GPS delay, currently unused
t_GPS = 250;

% initialize new variables
% first col: timestamps finish line crossing
% second ciol: laptimes
x = 100;
LapsRaw = zeros(x, 3);

%figure('Name', 'GPS');
%scatter(Lon, Lat);
%xlabel('Lon');
%ylabel('Lat');

iCurrent = 1;

for i = 2:(End - Start)
    current = Lon(i);
    last = Lon(i - 1);

    if (current > FinLon && last < FinLon)

        if (Lat(i) < FinLatMax && Lat(i) > FinLatMin)
            iLast = iCurrent;
            iCurrent = i;
            NewLaptime = (t(iCurrent) - t(iLast));
            LapsRaw(LapsCount + 1, 1) = LapsCount;
            LapsRaw(LapsCount + 1, 2) = iLast;
            LapsRaw(LapsCount + 1, 3) = iCurrent;
            LapsCount = LapsCount + 1;
        end

        % captures case finish line is crossed in opposite direction then
        % assumed by variables declaration
    elseif (current < FinLon && last > FinLon)

        if (Lat(i) > FinLatMax && Lat(i) < FinLatMin)
            iLast = iCurrent;
            iCurrent = i;
            NewLaptime = (t(iCurrent) - t(iLast));
            LapsRaw(LapsCount + 1, 1) = LapsCount;
            LapsRaw(LapsCount + 1, 2) = iLast;
            LapsRaw(LapsCount + 1, 3) = iCurrent;
            LapsCount = LapsCount + 1;
        end

    end

end

%% calculations

% deletes first row which indicates time until Finish Line
% ist reached the first time (lap zero)
LapsRaw(1, :) = [];

% calculates laptime regarding to VCU cycle time
LapsRaw(:, 4) = 0.005 * (LapsRaw(:, 3) - LapsRaw(:, 2));

% deletes invalid laps regarding to too fast times (below 30s)
LapsRaw(LapsRaw(:, 4) <= 30, :) = [];

% count invalid laps including lap zero
LapsRawRow = size(LapsRaw, 1);
LapsTrash = LapsCount - LapsRawRow;

% adds the following values during lap:
%  5: average speed in km/h
%  6: lap distance in m
%  7: time on full throttle in percentage of laptime
%  8: average throttle position under accelerating in percentage
%  9: average positive throttle speed without full throttle phases in %/s
% 10: time under full branking in percentage of laptime
% 11: average brakeforce under braking in N
% 12: brake release smoothness in N/s
% 13: average brake agressiveness in N/s (brakeforcedeviations above certain
%     level - 6000 N/s)
% 14: average steering smoothness in �/s
% 15: average curvature in 1/km
% 16: setted power limit in kW
% 17: average power in kW
% 18: time of full power in percentage of lap time

for i = 1:size(LapsRaw, 1)
    lapborders = LapsRaw(i, 2):LapsRaw(i, 3);
    LapsRaw(i, 5) = mean(KF_Geschwindigkeit(lapborders));
    LapsRaw(i, 6) = LapsRaw(i, 4) * LapsRaw(i, 5) / 3.6;

    GP = Gaspedal(lapborders);
    LapsRaw(i, 7) = 0.005 * 100 / LapsRaw(i, 4) * length(lapborders(GP > 0.95));
    LapsRaw(i, 8) = mean(GP(GP > 0.05)) * 100;
    GP_gradient = gradient(GP, 0.005) * 100;
    GP_gradient(GP > 0.99) = [];
    GP_gradient(GP_gradient < 0) = [];
    LapsRaw(i, 9) = mean(GP_gradient);

    BF = Brakeforce(lapborders);
    LapsRaw(i, 10) = 0.005 * 100 / LapsRaw(i, 4) * length(t(BF > 0.95 * 1200));
    LapsRaw(i, 11) = mean(BF(BF > 150));
    BF_speed = gradient(BF, 0.005);
    LapsRaw(i, 12) = abs(mean(BF_speed(BF_speed < 0)));
    LapsRaw(i, 13) = mean(BF_speed(BF_speed > 2500));

    LapsRaw(i, 14) = mean(abs(gradient(Lenkwinkel(lapborders), 0.005)));

    curvature = KF_Geschwindigkeit(lapborders) .* KF_Geschwindigkeit(lapborders) ./ (abs(Fr_ACC_Y(lapborders)) * 3.6 * 3.6);
    curvature = curvature.^(-1);
    curvature(curvature > 1) = 1;
    LapsRaw(i, 15) = 1000 * mean(curvature);

    LapsRaw(i, 16) = max(allowedPower(lapborders));
    LapsRaw(i, 17) = mean(actualPower(lapborders));
    LapsRaw(i, 18) = 0.005 * 100 / LapsRaw(i, 4) * length(t(actualPower(lapborders) > 0.95 * max(allowedPower(lapborders))));
end

% best laps regarding to similar procedure as kart and sim
BestLapsRaw = zeros(length(LapsRawRow) - 1, 4);

if LapsRawRow >= 3

    for j = 3:LapsRawRow
        BestLapsRaw(j - 2, 1) = mean([LapsRaw(j - 2, 4), LapsRaw(j - 1, 4), LapsRaw(j, 4)]);
        BestLapsRaw(j - 2, 2) = min([LapsRaw(j - 2, 4), LapsRaw(j - 1, 4), LapsRaw(j, 4)]);
        BestLapsRaw(j - 2, 3) = std([LapsRaw(j - 2, 4), LapsRaw(j - 1, 4), LapsRaw(j, 4)]);
        BestLapsRaw(j - 2, 4) = 0.66 * BestLapsRaw(j - 2, 2) + 0.33 * BestLapsRaw(j - 2, 1) + 0.01 * BestLapsRaw(j - 2, 3);
    end

    [BestValue, idx] = min(BestLapsRaw(:, 4));
    BestLaps(:, 1) = LapsRaw(idx:idx + 2, 1);
    BestLaps(:, 2) = LapsRaw(idx:idx + 2, 4);
    BestLaps(:, 3) = LapsRaw(idx:idx + 2, 15);
    BestLaps(:, 4) = LapsRaw(idx:idx + 2, 7);
    BestLaps(:, 5) = LapsRaw(idx:idx + 2, 11);
else
    errordlg('Fewer than 3 laps in data', 'Error');
end

progress(1) = min(BestLaps(:, 2));
progress(2) = mean(BestLaps(:, 2));
progress(3) = std(BestLaps(:, 2));
progress(4) = mean(BestLaps(:, 3));
progress(5) = mean(BestLaps(:, 4));
progress(6) = mean(BestLaps(:, 5));
progress = array2table(progress);
progress.Properties.VariableNames = {'min_laptime_s', 'avg_laptime_s', 'std_laptime_s', 'avg_curvature_1_km', 'avg_ftt_perc', 'avg_brakeforce_N'};

% compress dataset to useful laps for driver evaluation
% (not slower than 107% of the fastest lap)
Laps = LapsRaw(LapsRaw(:, 4) < 1.07 * min(LapsRaw(:, 4)), :);
LapsToSlow = LapsRaw(LapsRaw(:, 4) >= 1.07 * min(LapsRaw(:, 4)), :);

% save lap matrix
save(strcat(erase(File, '.mat'), '_laps'), 'BestLaps', 'LapsRaw', 'Laps', 'LapsTrash', 'LapsToSlow', 'progress', 'Filepath');

[temp, sorti] = sort(Laps(:, 4));
LapsSort = Laps(sorti, :);

%% plots

figure('Name', 'stint overview: lap times', 'NumberTitle', 'off');
ax1 = subplot(2, 1, 1);
b = bar(LapsRaw(:, 4));
b.FaceColor = 'flat';

for k = LapsToSlow(:, 1)'
    b.CData(k, :) = [1 0 0];
end

title('raw lap times');
ylim([min(LapsRaw(:, 4)) - 1, max(LapsRaw(:, 4)) + 1])
ylabel("time in s")
xticks(1:size(LapsRaw, 1))
xticklabels(LapsRaw(:, 1))
ax2 = subplot(2, 1, 2);
bar(Laps(:, 4))
title('filtered laptimes (slower than 107% of fastest stint lap time)');
ylim([min(Laps(:, 4)) - 1, max(Laps(:, 4)) + 1])
ylabel("time in s")
xticks(1:size(Laps, 1))
xticklabels(Laps(:, 1))
linkaxes([ax1, ax2], 'x');
Text1(1, 1) = "laps trashed - "+ string(LapsTrash);
Text1(2, 1) = "laps done - "+ string(LapsRaw(end, 1));
Text1(3, 1) = "laps to slow - "+ string(size(LapsToSlow, 1));
Text1(4, 1) = "fastest laptime - "+ string(min(Laps(:, 4)));
annotation('textbox', [0 0.7 .3 .2], 'String', Text1, 'EdgeColor', 'none', 'Color', 'k');

if LapsRawRow >= 3
    figure('Name', 'stint overview: best three related laps', 'NumberTitle', 'off');
    bar(BestLaps(:, 2))
    xticklabels(BestLaps(:, 1))
    ylim([min(BestLaps(:, 2)) - 0.25, max(BestLaps(:, 2)) + 0.25])
    ylabel('time in s')
    title('best related laptimes (from raw laps)')
    Text2(1:3, 1) = string(BestLaps(:, 2));
    Text2(4, 1) = "avg - "+ string(BestLapsRaw(idx, 1));
    Text2(5, 1) = "min - "+ string(BestLapsRaw(idx, 2));
    Text2(6, 1) = "std - "+ string(BestLapsRaw(idx, 3));
    Text2(7, 1) = "sum - "+ string(BestLapsRaw(idx, 4));
    annotation('textbox', [0 0.7 .3 .2], 'String', Text2, 'EdgeColor', 'none', 'Color', 'k');
end

figure('Name', 'stint overview: multiple values over sorted lap times', 'NumberTitle', 'off');
subplot(2, 1, 1);
bar(LapsSort(:, 4))
xticks(1:size(Laps, 1))
xticklabels(LapsSort(:, 1))
ylim([min(LapsSort(1, 4)) - 1, LapsSort(end, 4) + 1])
ylabel("time in s")

figure('Name', 'GPS track overview', 'NumberTitle', 'off');
scatter(Lon, Lat);
ylabel('Latitude');
xlabel('Longitude');

%% searching trends

figure('Name', 'stint overview: searching trends', 'NumberTitle', 'off');
hold on

for d = [5, 7, 10, 14, 15]
    scatter(LapsRaw(:, 4), LapsRaw(:, d));
end

hold off
xlim([40, 50])
xlabel('laptime in s')
legend('average vehicle velocity [km/h]', 'full throttle time to laptime ratio [%]', 'full braking time to laptime ratio [%]', 'average steering smoothness [�/s]', 'average curvature [1/km]')

%% clear workspace
clearvars('-except', 'File');
load(strcat(erase(File, '.mat'), '_laps'));
