% compares Ref lap with another lap
% neccessary before: doing the LAPS script with the own laps

%% name of Ref lap and calculations
RefLaps = 'Amadeus2018_07_25_19_05_08_Julian_laps';
RefLap = 3;

RefQuest = questdlg(['Wanna change reference lap?',newline,'current reference:',newline,RefLaps,newline,'lap: ',num2str(RefLap)],'refernece setting','No');

if length(RefQuest) == 3
    [RefFile,Folderpath] = uigetfile;
    if RefFile == 0
        errordlg('No data selected! Repeat script start.','invalid input')
        return
    end 
    RefFilepath = strcat(Folderpath,RefFile);
    load(RefFilepath);
    msg = msgbox(['Available laps:',newline,mat2str(Laps(:,1))],'lap counter');
    RefLap = inputdlg('Set number of reference lap of chosen dataset:','lap setting');
    RefLap = str2double(RefLap{1});
    delete(msg);
    if ~ any(Laps(:,1) == RefLap)
        errordlg('Lap number not permitted! Script stops.','invalid input')
        return
    end
    load(Filepath);
elseif length(RefQuest) == 2
   load(RefLaps);
   load(Filepath);
else
    errordlg('Choose wheater Yes or No!','invalid input')
    return
end

iRef = find(Laps(:,1) == RefLap);
RefDistance = cumsum(KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3))*0.005/3.6);
RefGP_speed = movmean(diff(Gaspedal)/0.005,10);
RefGP_speed(RefGP_speed < 0) = 0;
RefFTT = zeros(length(Gaspedal),1);
RefFTT(Gaspedal > 0.95*max(Gaspedal)) = 0.005;
RefBP_speed = abs(movmean(diff(Brakeforce)/0.005,10));
RefACC_BE = Fr_ACC_X;
RefACC_BE(RefACC_BE > 0) = 0;

LapsCompared(1,:) = Laps(iRef,:); 

%evaluate time difference over lap
%evaluate distance of Ref lap
distance = cumsum(KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)))*0.005/3.6;

%trash double existing values
[distanceIntRef, indexRef] = unique(distance);

%% figure 1 Ref data Gaspedal

f1 = figure('Name','lap comparison: Gaspedal','NumberTitle','off');
ax1(1) = subplot(3,1,1);
ax1(2) = subplot(3,1,2);
ax1(3) = subplot(3,1,3);

TextRef(1,1) = "REF LAP:";
TextRef(2,1) = "laptime - " + string(round(Laps(iRef,4),2)) + "s";
TextRef(3,1) = "FT time - " + string(round(Laps(iRef,7),2)) + "%";
TextRef(4,1) = "TP avg uA - " + string(round(Laps(iRef,8),2)) + "%";
annotation('textbox',[0 0.8 .1 .2],'String',TextRef,'EdgeColor','none','Color','r');

subplot(ax1(1));
plot(RefDistance,KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("vehiclespeed" + newline + "in km/h");
yyaxis right
set(gca,'YColor','k')
plot(RefDistance,cumsum(RefFTT(Laps(iRef,2):Laps(iRef,3))),'r--');
ylim([0,1+max(cumsum(RefFTT(Laps(iRef,2):Laps(iRef,3))))]);
ylabel("full throttle time" + newline + "in s");

subplot(ax1(2));
plot(RefDistance,100*Gaspedal(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("gaspedalposition" + newline + "in %");

subplot(ax1(3));
plot(RefDistance,100*RefGP_speed(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("gaspedalspeed" + newline + "in %/s");
xlabel('track distance in metres');

%% figure 2 Ref data Brakepedal

f2 = figure('Name','lap comparison: Brakes','NumberTitle','off');
ax2(1) = subplot(3,1,1);
ax2(2) = subplot(3,1,2);
ax2(3) = subplot(3,1,3);

TextRef2(1,1) = "REF LAP:";
TextRef2(2,1) = "laptime - " + string(round(Laps(iRef,4),2)) + "s";
TextRef2(3,1) = "FB time - " + string(round(Laps(iRef,9),2)) + "s";
TextRef2(4,1) = "BF avg uB - " + string(round(Laps(iRef,10),2)) + "N";
annotation('textbox',[0 0.8 .1 .2],'String',TextRef2,'EdgeColor','none','Color','r');

subplot(ax2(1));
plot(RefDistance,KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("vehiclespeed" + newline + "in km/h");

subplot(ax2(2));
plot(RefDistance,Brakeforce(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("brakeforce" + newline + "in N");
ylim([0,1200]);
yyaxis right
set(gca,'YColor','k')
plot(RefDistance,BP_Front(Laps(iRef,2):Laps(iRef,3))+BP_Rear(Laps(iRef,2):Laps(iRef,3)),'r--');
ylim([0,max(BP_Front(Laps(iRef,2):Laps(iRef,3))+BP_Rear(Laps(iRef,2):Laps(iRef,3)))+15]);
ylabel("sum brake pressure" + newline + "in bar");

subplot(ax2(3));
plot(RefDistance,RefBP_speed(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("brakeforcedeviation" + newline + "in N/s");
yyaxis right
set(gca,'YColor','k')
plot(RefDistance,movmean(RefACC_BE(Laps(iRef,2):Laps(iRef,3)),5)/9.81,'r--');
ylim([-2,0]);
ylabel("brakeeffort" + newline + "in g");
xlabel('track distance in metres');

%% figure 3 Ref data Traction Circle

f3 = figure('Name','lap comparison: traction circle','NumberTitle','off');
scatter(Fr_ACC_Y(Laps(iRef,2):Laps(iRef,3))/9.81,Fr_ACC_X(Laps(iRef,2):Laps(iRef,3))/9.81,'r');
xvalue = linspace(-2,2);
yvalue = sqrt(4-xvalue.^2);
hold on 
for scale = linspace(0.1,1,10)
plot(scale*xvalue,scale*yvalue,'Color',1/255*[100,100,100]);
plot(scale*xvalue,-scale*yvalue,'Color',1/255*[100,100,100]);
end
hold off
xlabel('lateral acceleration in g');
ylabel('longitudinal acceleration in g');

%% figure 4 Ref data Steering Wheel and Driving Line

f4 = figure('Name','lap comparison: Steering and Driving Line','NumberTitle','off');
ax4(1) = subplot(3,1,1);
ax4(2) = subplot(3,1,2);
ax4(3) = subplot(3,1,3);

subplot(ax4(1));
plot(RefDistance,KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("vehiclespeed" + newline + "in km/h");

subplot(ax4(2));
curvature = KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)).*KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3))/(3.6*3.6);
curvature = curvature./abs(Fr_ACC_Y(Laps(iRef,2):Laps(iRef,3)));
curvature = curvature.^(-1);
plot(RefDistance,1000*movmean(curvature,20),'r');
ylabel("curvature" + newline + "in 1/km");

subplot(ax4(3));
plot(RefDistance,Lenkwinkel(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("steering angle" + newline + "in degrees");
yyaxis right
set(gca,'YColor','k')
plot(RefDistance,gradient(Lenkwinkel(Laps(iRef,2):Laps(iRef,3)),0.005),'r--');
ylabel("steering speed" + newline + "in �/s");
xlabel('track distance in metres');

%% figure 5 time difference over lap

f5 = figure('Name','lap comparison: time difference','NumberTitle','off');
ax5(1) = subplot(2,1,1);
ax5(2) = subplot(2,1,2);

subplot(ax5(1));
plot(RefDistance,KF_Geschwindigkeit(Laps(iRef,2):Laps(iRef,3)),'r');
ylabel("vehiclespeed" + newline + "in km/h");

%% figure 7 Ref data Track Map

f7 = figure('Name','lap comparison: track map','NumberTitle','off');
plot(GPS_Fr_Longitude(Laps(iRef,2):Laps(iRef,3)),GPS_Fr_Latitude(Laps(iRef,2):Laps(iRef,3)),'r');
hold on
iFTT = find(RefFTT >0);
iFTT(iFTT < Laps(iRef,2)) = [];
iFTT(iFTT > Laps(iRef,3)) = [];
scatter(GPS_Fr_Longitude(iFTT),GPS_Fr_Latitude(iFTT),'r','.')
iBP = find(RefBP_speed >4000);
iBP(iBP < Laps(iRef,2)) = [];
iBP(iBP > Laps(iRef,3)) = [];
scatter(GPS_Fr_Longitude(iBP),GPS_Fr_Latitude(iBP),'r','x')
hold off

%% own lap (the one that should be compared) and calculations
OwnLaps = 'Amadeus2019_05_05_14_51_04_laps';
OwnLap = 23;

OwnQuest = questdlg(['Wanna load different lap to compare?',newline,'current lap:',newline,OwnLaps,' lap: ',num2str(OwnLap)],'own lap setting','Yes');

if length(OwnQuest) == 3
    [OwnFile,Folderpath] = uigetfile;
    if OwnFile == 0
        errordlg('No data selected! Repeat script start.','invalid input')
        return
    end 
    OwnFilepath = strcat(Folderpath,OwnFile);
    load(OwnFilepath);
    msg = msgbox(['Available laps:',newline,mat2str(Laps(:,1))],'lap counter');
    OwnLap = inputdlg('Set number of the lap to be compared:','lap setting');
    OwnLap = str2double(OwnLap{1});
    delete(msg);
    if ~ any(Laps(:,1) == OwnLap)
        errordlg('Lap number not permitted! Script stops.','invalid input')
        return
    end
    load(Filepath);
elseif length(OwnQuest) == 2
   load(OwnLaps);
   load(Filepath);
else
    errordlg('Choose wheater Yes or No!','invalid input')
    return
end


iOwn = find(Laps(:,1) == OwnLap);
OwnDistance = cumsum(KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3))*0.005/3.6);
faktorDistance = RefDistance(length(RefDistance))/OwnDistance(length(OwnDistance));
OwnDistance =  OwnDistance*faktorDistance;
OwnGP_velocity = movmean(diff(Gaspedal)/0.005,10);
OwnGP_velocity(OwnGP_velocity < 0) = 0;
OwnFTT = zeros(length(Gaspedal),1);
OwnFTT(Gaspedal > 0.95*max(Gaspedal)) = 0.005;
OwnBP_speed = abs(movmean(diff(Brakeforce)/0.005,10));
OwnACC_BE = Fr_ACC_X;
OwnACC_BE(OwnACC_BE > 0) = 0;

LapsCompared(2,:) = Laps(iOwn,:);

%time difference evaluation over lap
%add own distance

distance = [distance;cumsum(KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)))*0.005/3.6];

%trash double existing values
[distanceInt, index] = unique(cumsum(KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)))*0.005/3.6);

%sort and delete double (or more) distance values as basis for interpolation
distance = sort(unique(distance));


% interpolated time for linked distance between laps
% time values results of between original distance, reffering time and the
% interlinked distance (variable: distance)
timeIntRef = interp1(distanceIntRef,(0.005*(indexRef - 1)),distance);
timeInt = interp1(distanceInt,(0.005*(index - 1)),distance);



%% figure 1 Own data
figure(f1)

TextOwn(1,1) = "OWN LAP:";
TextOwn(2,1) = "laptime - " + string(round(Laps(iOwn,4),2)) + "s";
TextOwn(3,1) = "FT time - " + string(round(Laps(iOwn,7),2)) + "%";
TextOwn(4,1) = "TP avg uA - " + string(round(Laps(iOwn,8),2)) + "%";
annotation('textbox',[0 0.6 .1 .2],'String',TextOwn,'EdgeColor','none');

subplot(ax1(1));
hold on
yyaxis left
plot(OwnDistance,KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)),'k');
yyaxis right
plot(OwnDistance,cumsum(OwnFTT(Laps(iOwn,2):Laps(iOwn,3))),'k--');
hold off
subplot(ax1(2));
hold on
plot(OwnDistance,100*Gaspedal(Laps(iOwn,2):Laps(iOwn,3)),'k');
hold off
subplot(ax1(3));
hold on
plot(OwnDistance,100*OwnGP_velocity(Laps(iOwn,2):Laps(iOwn,3)),'k');
hold off
legend('reference lap','own lap');

linkaxes([ax1(1),ax1(2),ax1(3)],'x');

%% figure 2 Own data
figure(f2)

TextOwn(1,1) = "OWN LAP:";
TextOwn(2,1) = "laptime - " + string(round(Laps(iOwn,4),2)) + "s";
TextOwn(3,1) = "FT time - " + string(round(Laps(iOwn,9),2)) + "s";
TextOwn(4,1) = "BF avg uB - " + string(round(Laps(iOwn,10),2)) + "N";
annotation('textbox',[0 0.6 .1 .2],'String',TextOwn,'EdgeColor','none');

subplot(ax2(1));
hold on
plot(OwnDistance,KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)),'k');
hold off
legend('reference lap','own lap');
subplot(ax2(2));
hold on
yyaxis left
plot(OwnDistance,Brakeforce(Laps(iOwn,2):Laps(iOwn,3)),'k');
yyaxis right
plot(OwnDistance,BP_Front(Laps(iOwn,2):Laps(iOwn,3))+BP_Rear(Laps(iOwn,2):Laps(iOwn,3)),'k--');
hold off
subplot(ax2(3));
hold on
yyaxis left
plot(OwnDistance,OwnBP_speed(Laps(iOwn,2):Laps(iOwn,3)),'k');
yyaxis right
plot(OwnDistance,movmean(OwnACC_BE(Laps(iOwn,2):Laps(iOwn,3)),5)/9.81,'k--');
hold off

linkaxes([ax2(1),ax2(2),ax2(3)],'x');

%% figure 3 Own data
figure(f3)
hold on
scatter(Fr_ACC_Y(Laps(iOwn,2):Laps(iOwn,3))/9.81,Fr_ACC_X(Laps(iOwn,2):Laps(iOwn,3))/9.81,'k');;
axis equal
hold off
legend('reference lap');

%% figure 4 Own data
figure(f4)
subplot(ax4(1));
hold on
plot(OwnDistance,KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)),'k');
hold off

subplot(ax4(2));
hold on
curvatureOwn = KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)).*KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3))/(3.6*3.6);
curvatureOwn = curvatureOwn./abs(Fr_ACC_Y(Laps(iOwn,2):Laps(iOwn,3)));
curvatureOwn = curvatureOwn.^(-1);
plot(OwnDistance,1000*movmean(curvatureOwn,20),'k');
hold off
legend('reference lap','own lap');

subplot(ax4(3));
hold on
yyaxis left
plot(OwnDistance,Lenkwinkel(Laps(iOwn,2):Laps(iOwn,3)),'k');
yyaxis right
plot(OwnDistance,gradient(Lenkwinkel(Laps(iOwn,2):Laps(iOwn,3)),0.005),'k--');
hold off

linkaxes([ax4(1),ax4(2),ax4(3)],'x');

%% figure 5 time difference over lap

figure(f5)

subplot(ax5(1));
hold on
plot(OwnDistance,KF_Geschwindigkeit(Laps(iOwn,2):Laps(iOwn,3)),'k');
legend('Ref','Own') %Ref,Own
hold off

subplot(ax5(2));
plot(distance,(timeInt - timeIntRef),'k');
legend('Time gap to reference'); %'Time gap to reference'
ylabel("time gap" + newline + "in s");
xlabel('track distance in metres');

linkaxes([ax5(1),ax5(2)],'x');

%% figure 7 Own data
figure(f7)
hold on
plot(GPS_Fr_Longitude(Laps(iOwn,2):Laps(iOwn,3)),GPS_Fr_Latitude(Laps(iOwn,2):Laps(iOwn,3)),'k');
iFTT = find(OwnFTT >0);
iFTT(iFTT < Laps(iOwn,2)) = [];
iFTT(iFTT > Laps(iOwn,3)) = [];
scatter(GPS_Fr_Longitude(iFTT),GPS_Fr_Latitude(iFTT),'k','.');
iBP = find(RefBP_speed >4000);
iBP(iBP < Laps(iOwn,2)) = [];
iBP(iBP > Laps(iOwn,3)) = [];
scatter(GPS_Fr_Longitude(iBP),GPS_Fr_Latitude(iBP),'k','x')
hold off
axis off
legend('reference lap line','full throttle (FT) phases','braking zones');



