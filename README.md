<div align="center">
<h1>LapEvaluation | Elbflorace EV (future DV)</h1>
<em>Warning: highly unstable scripts! If your computer catches fire you are on your own</em></br></br>
</div>


## Instructions

0. get data (like `.mat`)
0. run `_old/laps_converter.m` on your data. **Warning:** it has to contain more than 3 laps, otherwise evaluation will fail
0. repeat previous step to get evaluation for a different file
0. now you should have 2 `_laps.mat` in your current directory
0. run `lap_evaluation.m` providing those paths
0. enjoy
